package org.example;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MainTest {

    @Test
    void testSum(){
        int sum = Main.sum(5, 10);
        assertEquals(15, sum);
    }
}